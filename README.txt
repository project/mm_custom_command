
Media Mover Custom Command
**************************

Allow user to define custom commands for process, storage and complete actions.
Module uses tokens to provide easy access to file values.

Dependencies
============
- media_mover_api
- mm_token

Install
=======

1) Copy the mm_custom_command folder to the modules folder in your installation.

2) Enable the module using Administer -> Modules (/admin/build/modules).

3) Go to Media Mover configuration where you want to use custom command and 
   choose "Custom Command: Use custom command".

4) Set 'Output file' and 'Command'.

Examples
========

Encode files with Sorenson Squeeze 5 on Windows machine running Drupal
----------------------------------------------------------------------
- Create your Media Mover configuration
- For process action choose "Custom Command: Use custom command"
- Set 'Output file' for example 'video-[nid].flv'
- Set command. Because I didn't find parameter for Squeeze to set output file I
  will use batch file 'compress-video.bat' which will after encoding rename
  output file. So command will look like this:
  'd:\compress-video.bat [harvest_file] [output_file]'

Rename/move file
----------------
- Create your Media Mover configuration
- For storage action choose "Custom Command: Use custom command"
- Set 'Output file' for example 'video-[nid].flv'
- Set 'Command' to 'co [process_file] [output_file]'

....

Todo:
=====
- make validation of config values
- create permissions to set if user can use Custom Command or not
- make documentation
